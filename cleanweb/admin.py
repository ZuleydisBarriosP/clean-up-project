from django.contrib import admin

from .models import Profile, User, Type_Service, Service, Booking, Asignacione
from django.contrib.auth.models import Permission
from django.contrib.auth.admin import UserAdmin

# Register your models here.


class ProfileInline(admin.StackedInline):
	model = Profile

class CustomUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        (
            None, {
                'fields': (
                    'identificacion',
                    'cell_phone',
                    'address',
                )
            }
        ),
    )



@admin.register(User)
class UserAdmin(CustomUserAdmin):
    inlines = [ProfileInline]
    list_display = ('id', 'first_name', 'last_name','address', 'email','cell_phone','is_active')
    search_fields = ('id', 'first_name', 'last_name', 'email')
@admin.register(Booking)
class Booking(admin.ModelAdmin):
    list_display = ('id', 'fecha', 'type_service', 'price', 'usuario')

@admin.register(Asignacione)
class Detail_Booking(admin.ModelAdmin):
    list_display = ('id_booking', 'id_service', 'id_user')

@admin.register(Type_Service)
class Type_Service(admin.ModelAdmin):
    list_display = ('id', 'type_service')

@admin.register(Service)
class Service(admin.ModelAdmin):
    list_display = ('id', 'type_service', 'description', 'price', 'state', 'stock')



admin.site.register(Permission)
