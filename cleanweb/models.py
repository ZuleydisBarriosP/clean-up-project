from django.contrib.auth.models import AbstractUser
from django.db import models


class Type_User(models.Model):
    type_user = models.CharField(max_length=50)

    def __str__(self):
        return str(self.type_user)


class User(AbstractUser):
    identificacion = models.IntegerField(null=True, blank=True)
    address = models.CharField(max_length=150, blank=True, null=True)
    cell_phone = models.CharField(max_length=150, blank=True, null=True)
    type_user = models.ForeignKey(Type_User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False)
    is_user = models.BooleanField(default=False)
    is_employee = models.BooleanField(default=False)

    def __str__(self):
        return f'Profile {self.user.username}'


class Type_Service(models.Model):
    type_service = models.CharField(max_length=50)

    def __str__(self):
        return self.type_service


class Service(models.Model):
    type_service = models.ForeignKey(Type_Service, on_delete=models.CASCADE)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    state = models.BooleanField(default=True)
    stock = models.IntegerField()

    def __str__(self):
        return str(self.price)


ASIGNACIONES_CHOICES = (
    ("1", "Pendiente"),
    ("2", "Asignado"),
    ("3", "Rechazado"),
)


class Booking(models.Model):
    fecha = models.DateField(auto_now=False)
    type_service = models.ForeignKey(Type_Service, on_delete=models.CASCADE)
    price = models.ForeignKey(Service, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    state = models.CharField(max_length=50, choices=ASIGNACIONES_CHOICES, default='1')

    def __str__(self):
        return str(self.fecha)


class Asignacione(models.Model):
    id_booking = models.ForeignKey(Booking, on_delete=models.CASCADE)
    id_service = models.ForeignKey(Service, on_delete=models.CASCADE)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id_booking)

